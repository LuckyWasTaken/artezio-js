function range(a, b) { // Функция-хелпер, имитирующая питоновский range
    return Array(b - a + 1).fill().map((_, i) => a + i)
}

function getInput() {
    let x1 = document.getElementById('x1').value;
    let x2 = document.getElementById('x2').value;


    if(!x1 || !x2) {
        alert("Поля x1 и x2 должны быть заполнены");
        return;
    }

    x1 = parseInt(x1);
    x2 = parseInt(x2);

    if(Number.isNaN(x1) || Number.isNaN(x2)) {
        alert("В поля x1 и x2 должны быть введены числовые значения");
        return;
    }

    return [x1, x2];
}

function buttonClick() {
    let x1, x2;
    [x1, x2] = getInput()


    const currentFunc = document.querySelector('input[name="func"]:checked').value;

    // const result = ((x2 - x1 + 1)*(x2+x1))/2; //без es6, математикой
    let result;
    if(currentFunc === "sum") {
        result = range(x1, x2).reduce((a, b) => a + b, 0); //с es6, без знания математики, но красиво! можно было тернарниками, но как по мне, иметь их два на одну строку кода - практика не оч
    } else {
        result = range(x1, x2).reduce((a, b) => a * b, 1);
    }

    const resultDiv = document.getElementById('result');
    resultDiv.innerText = currentFunc === "sum"
        ? "Сумма чисел от x1 до x2 = " + result
        : "Произведение чисел от x1 до x2 = " + result;
}

function clearButtonClick() {
    document.getElementById('x1').value = "";
    document.getElementById('x2').value = "";
}

function primeButtonClick() {
    let x1, x2;
    [x1, x2] = getInput();

    const resultDiv = document.getElementById('result');
    resultDiv.innerText = findPrimes(x1, x2).reduce((a, b)=> a + ' ' + b, '');
}

function findPrimes(start, end) {
    if (end < 2) return []
    let primes = [2];
    for (let i = 2; i < end; i++) {
        if(primes.every((val) => i/val % 1 !== 0)) {
            primes.push(i);
        }
    }
    return primes.filter((val) => val >= start);
}
